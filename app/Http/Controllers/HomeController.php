<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB as FacadesDB;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{

    public function index()
    {
        return view('home.index');
    }

    public function mensaje()
    {
        //logica de control
        return view('home.mensaje',[
            'mensaje' => 'Aplicacion 3 con Laravel'
        ]);
    }

    public function listado()
    {
        //logica de control
        // $datos=[
        //     'Ramon',
        //     'Juan',
        //     'Maria',
        //     'Pedro',
        // ];

        //saco los nombre de la base de datos
        $datos=DB::table('nombres')->pluck('nombre');

        return view('home.listado',['datos'=>$datos]);
    }

    public function imagen()
    {

        return view('home.imagen');
    }
}
