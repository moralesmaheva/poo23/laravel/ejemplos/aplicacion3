<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NombresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('nombres')->insert([
            ['nombre' => 'Ramon'],
            ['nombre' => 'Juan'],
            ['nombre' => 'Maria'],
        ]);
    }
}
