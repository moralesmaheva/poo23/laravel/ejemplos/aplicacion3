<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

//ruta para index co controlador HomeController
// /=>HomeController.index
Route::get('/', [HomeController::class, 'index']);
//ruta con controlador/accion
Route::get('/home', [HomeController::class, 'index']);
//ruta con controlador/accion
Route::get('/index', [HomeController::class, 'index']);

//ruta para listado
// /listado=>HomeController.listado
//Route::get('/listado', [HomeController::class, 'listado']);
//ruta con controlador/accion
Route::get('/home/listado', [HomeController::class, 'listado']);

//ruta para imagen
// /imagen=>HomeController.imagen
//Route::get('/imagen', [HomeController::class, 'imagen']);
//ruta con controlador/accion
Route::get('/home/imagen', [HomeController::class, 'imagen']);

//ruta para mensaje
// /mensaje=>HomeController.mensaje
//Route::get('/mensaje', [HomeController::class, 'mensaje']);
//ruta con controlador/accion
Route::get('/home/mensaje', [HomeController::class, 'mensaje']);
